import { BluetoothProvider } from "@providers";

import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import { COLORS } from "@styles/colors";

import React from "react";

import "react-native-gesture-handler";

import { DefaultTheme, Provider as PaperProvider } from "react-native-paper";
import { Theme } from "react-native-paper/lib/typescript/types";
import {
  useFonts,
  Roboto_500Medium,
  Roboto_400Regular,
} from "@expo-google-fonts/roboto";
import { Views } from "./views";

const Stack = createStackNavigator();

const theme: Theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: COLORS.GOOD,
    accent: COLORS.GOOD,
  },
  mode: "exact",
};

/**
 * TODO: Implement auth protection
 * TODO: Implement global THEME context (lol)
 */

export default function App() {
  const [fontLoaded] = useFonts({
    Roboto_500Medium,
    Roboto_400Regular,
  });

  return (
    <BluetoothProvider>
      <PaperProvider theme={theme}>
        <NavigationContainer>
          <Stack.Navigator
            screenOptions={{ headerShown: false }}
            initialRouteName="Login"
          >
            {Object.values(Views).map((view) => (
              <Stack.Screen
                key={`${view}`}
                name={`${view}`}
                component={view.Component}
                options={view.options}
              />
            ))}
          </Stack.Navigator>
        </NavigationContainer>
      </PaperProvider>
    </BluetoothProvider>
  );
}
