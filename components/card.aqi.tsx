import React from "react";
import { View, Text, TextStyle, ViewStyle } from "react-native";
import { ColorsType } from "../styles/colors";

interface AQITextProps {
  variant: ColorsType;
  AQI: number;
}

export const CardAQI = ({ variant, AQI }: AQITextProps) => {
  const styles = useStyles({ variant });
  return (
    <View style={styles.aqiContainer}>
      <Text style={styles.aqiNumber}>{AQI}</Text>
      <Text style={styles.aqiText}>AQI</Text>
    </View>
  );
};

interface CardAQIStyle {
  aqiContainer: ViewStyle;
  aqiNumber: TextStyle;
  aqiText: TextStyle;
}

const useStyles = ({ variant }: Omit<AQITextProps, "AQI">): CardAQIStyle => ({
  aqiContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "baseline",
  },
  aqiNumber: {
    fontSize: 64,
    fontWeight: "700",
    color: variant,
  },
  aqiText: {
    fontSize: 36,
    fontWeight: "700",
    color: variant,
    paddingBottom: 8,
  },
});
