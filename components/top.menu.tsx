import React from "react";
import { Pressable, Text, View, ViewStyle } from "react-native";
import { COLORS, ColorsType } from "../styles/colors";
import SettingsIcon from "@icons/settings";
import AddIcon from "@icons/add";
import { useNavigation } from "@react-navigation/core";
import { Paths } from "@views/paths";

const TopMenuStyle: ViewStyle = {
  borderRadius: 9,
  borderWidth: 1,
  borderColor: COLORS.GOOD,
  backgroundColor: "#FFFFFF",
  marginBottom: 16,
  padding: 18,
  display: "flex",
  justifyContent: "space-between",
  flexDirection: "row",
  alignItems: "center",
};

interface TopMenuProps {
  variant: ColorsType;
}

export const TopMenu = ({ variant = COLORS.GOOD }: TopMenuProps) => {
  const { navigate } = useNavigation();

  return (
    <View style={TopMenuStyle}>
      <Pressable
        onPress={() => navigate(Paths.Settings)}
        android_ripple={{ color: variant }}
        style={{
          width: 28,
          height: 28,
          borderRadius: 28,
        }}
      >
        <SettingsIcon variant={variant} width={28} height={28} />
      </Pressable>
      <Pressable
        onPress={() => navigate(Paths.PairDeviceList)}
        android_ripple={{ color: variant }}
        style={{
          width: 28,
          height: 28,
          borderRadius: 28,
        }}
      >
        <AddIcon variant={variant} width={30} height={30} />
      </Pressable>
    </View>
  );
};
