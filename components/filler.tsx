import { View } from "react-native";
import React from "react";

export const Filler = () => {
  return <View style={{ flex: 1 }}></View>;
};
