import { useFonts, Roboto_500Medium } from "@expo-google-fonts/roboto";
import { COLORS } from "@styles/colors";
import React from "react";
import { Pressable, Text, TextStyle, View, ViewStyle } from "react-native";
import { lighten, darken } from "polished";
interface ButtonProps {
  onPress?: () => void;
  text: string;
  disabled?: boolean;
  styles?: ViewStyle;
  color?: string;
}

interface IButtonStyles {
  pressable: ViewStyle;
  text: TextStyle;
}

type StyleProps = Pick<ButtonProps, "disabled" | "color">;

const getStyle = ({ disabled, color }: StyleProps): IButtonStyles => ({
  pressable: {
    borderRadius: 9,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: disabled ? darken(0.3, color) : color,
  },
  text: {
    fontSize: 14,
    fontFamily: "Roboto_500Medium",
    color: "rgba(255,255,255,.8)",
    textTransform: "uppercase",
    padding: 13,
  },
});

export const Button = ({
  onPress,
  text,
  color = COLORS.GOOD,
  disabled = false,
  styles = {},
}: ButtonProps) => {
  const style = getStyle({ disabled, color });
  const [fontLoaded] = useFonts({
    Roboto_500Medium,
  });

  if (!fontLoaded) return null;

  return (
    <View style={{ borderRadius: 12, overflow: "hidden", ...styles }}>
      <Pressable
        disabled={disabled}
        style={style.pressable}
        onPress={onPress}
        android_ripple={{ color: lighten(0.1, color) }}
      >
        <Text style={style.text}>{text}</Text>
      </Pressable>
    </View>
  );
};
