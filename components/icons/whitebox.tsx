import React from "react";
import { View, ViewStyle } from "react-native";
import { COLORS } from "@styles/colors";

interface WhiteboxProps {
  children: JSX.Element | JSX.Element[];
}

const WhiteboxStyle: ViewStyle = {
  borderRadius: 9,
  borderWidth: 1,
  borderColor: COLORS.GOOD,
  backgroundColor: "#FFFFFF",
  display: "flex",
  flexDirection: "column",
  overflow: "hidden",
};

const Whitebox = ({ children }: WhiteboxProps) => {
  return <View style={WhiteboxStyle}>{children}</View>;
};

const WhiteboxElementStyle: ViewStyle = {
  marginBottom: 0,
  padding: 15,
};

export const Element = ({ children }: WhiteboxProps) => (
  <View style={WhiteboxElementStyle}>{children}</View>
);

export default Whitebox;
