import { SvgProps } from "react-native-svg";
import { ColorsType } from "../../styles/colors";

export interface IconProps extends SvgProps {
  variant: ColorsType;
}
