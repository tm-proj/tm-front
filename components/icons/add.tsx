import * as React from "react";
import Svg, { Path } from "react-native-svg";
import { COLORS } from "./../../styles/colors";
import { IconProps } from "./icon.props";

function SvgComponent({ variant, ...props }: IconProps) {
  return (
    <Svg
      width={24}
      height={24}
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M19 3H5a2 2 0 00-2 2v14a2 2 0 002 2h14a2 2 0 002-2V5a2 2 0 00-2-2zM12 8v8M8 12h8"
        stroke={variant ?? COLORS.GOOD}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  );
}

export default SvgComponent;
