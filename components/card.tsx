import React from "react";
import { View, Text, Pressable, ViewStyle, TextStyle } from "react-native";
import { calculateAQI, getSummaryText } from "@utils/aqi.calculator";
import { COLORS, ColorsType } from "@styles/colors";
import { CardAQI } from "@components/card.aqi";
import { Chips } from "@components/chips";
import { DateTime } from "luxon";

interface CardProps {
  name: string;
  variant: ColorsType;
  PM10: number;
  PM25: number;
  Temperature: number;
  Humidity: number;
  onPress?: () => void;
  disabled?: boolean;
  timestamp: number;
}

export const Card = ({
  name,
  onPress,
  PM10,
  PM25,
  Temperature,
  Humidity,
  timestamp,
  variant = COLORS.GOOD,
  disabled = false,
}: CardProps) => {
  const styles = useCardStyle({ variant });

  if (!PM10 || !PM25 || !Temperature || !Humidity || !timestamp) return null;

  const aqi = calculateAQI(PM10);
  return (
    <View style={styles.wrapper}>
      <View
        style={{
          borderRadius: 9,
          borderWidth: 0,
          borderColor: "#ffffff",
          overflow: "hidden",
        }}
      >
        <Pressable
          disabled={disabled}
          android_ripple={{ color: variant }}
          style={{
            backgroundColor: "#FFFFFF",
          }}
          onPress={() => onPress && onPress()}
        >
          <View style={styles.outerContainer}>
            <View style={styles.innerTopContainer}>
              <CardAQI variant={variant} AQI={aqi} />
              <View style={styles.deviceDetailsContainer}>
                <Text style={styles.deviceName}>{name ?? "Ładowanie"}</Text>
                <Text style={styles.measurementDate}>
                  {DateTime.fromSeconds(timestamp).toFormat("TT yyyy-MM-dd") ??
                    "Nieznany"}
                </Text>
              </View>
            </View>
            <Text style={styles.summaryText}>{getSummaryText(aqi)}</Text>
            <Chips
              PM10={PM10.toFixed(1)}
              PM25={PM25.toFixed(1)}
              variant={variant}
              Temperature={Temperature.toFixed(1)}
              Humidity={Humidity.toFixed(1)}
            />
          </View>
        </Pressable>
      </View>
    </View>
  );
};

interface CardStyle {
  wrapper: ViewStyle;
  outerContainer: ViewStyle;
  innerTopContainer: ViewStyle;
  deviceDetailsContainer: ViewStyle;
  deviceName: TextStyle;
  measurementDate: TextStyle;
  summaryText: TextStyle;
}

const useCardStyle = ({ variant }: Pick<CardProps, "variant">): CardStyle => ({
  wrapper: {
    paddingBottom: 15,
    borderRadius: 9,
    overflow: "hidden",
  },
  outerContainer: {
    borderRadius: 9,
    borderColor: variant,
    borderWidth: 1,
    paddingTop: 0,
    paddingBottom: 18,
    paddingLeft: 18,
    paddingRight: 18,
  },
  innerTopContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  deviceDetailsContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-end",
    paddingTop: 18,
  },
  deviceName: {
    color: variant,
    fontSize: 11,
  },
  measurementDate: {
    color: "#939393",
    fontSize: 9,
  },
  summaryText: {
    color: variant,
    fontSize: 18,
  },
});
