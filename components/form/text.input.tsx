import React, { useState } from "react";
import { useField } from "formik";
import { TextInputProps as BaseTextInputProps, View } from "react-native";
import {
  DefaultTheme,
  HelperText,
  TextInput as PaperInput,
} from "react-native-paper";
import { COLORS } from "@styles/colors";
import { darken } from "polished";
import { Theme } from "react-native-paper/lib/typescript/types";

interface TextInputProps extends BaseTextInputProps {
  readonly name: string;
  readonly label: string;
  readonly selectionColor?: string;
}

const theme: Theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    accent: COLORS.GOOD,
    primary: COLORS.GOOD,
    error: darken(0.3, COLORS.GOOD),
  },
};

export const TextInput = ({
  name,
  label,
  secureTextEntry,
  ...rest
}: TextInputProps) => {
  const [field, helpers, meta] = useField(name);
  const [visible, setVisible] = useState(false);

  return (
    <View style={{ position: "relative", marginBottom: 10 }}>
      <PaperInput
        mode="outlined"
        label={label}
        onChangeText={meta.setValue}
        onBlur={() => meta.setTouched(true)}
        value={field.value}
        error={!!helpers.error}
        selectionColor={COLORS.GOOD}
        {...rest}
        right={
          secureTextEntry && (
            <PaperInput.Icon
              color={(focused: boolean) =>
                !!helpers.error
                  ? theme.colors.error
                  : focused
                  ? COLORS.GOOD
                  : theme.colors.text
              }
              name={!visible ? "eye" : "eye-off"}
              onPress={() => setVisible((x) => !x)}
            />
          )
        }
        theme={theme}
        secureTextEntry={!visible && secureTextEntry}
      />
      <HelperText
        style={{ position: "absolute", bottom: -23 }}
        type="info"
        visible={!!helpers.error}
      >
        {helpers.error}
      </HelperText>
    </View>
  );
};
