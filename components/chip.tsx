import React from "react";
import { View, ViewStyle, Text, TextStyle } from "react-native";
import { ColorsType } from "../styles/colors";

export const ChipVariants = {
  Outlined: "OUTLINED",
  Filled: "FILLED",
} as const;

export type ChipVariantType = typeof ChipVariants[keyof typeof ChipVariants];

interface ChipStylesProps {
  variant: ChipVariantType;
  color: ColorsType;
}

const ChipStyles = ({ variant, color }: ChipStylesProps): ViewStyle => ({
  borderRadius: 54,
  borderColor: variant === ChipVariants.Outlined ? color : undefined,
  borderWidth: variant === ChipVariants.Outlined ? 1 : undefined,
  backgroundColor: variant === ChipVariants.Filled ? color : undefined,
  paddingVertical: 6,
  paddingHorizontal: 12,
  marginRight: 8,
  display: "flex",
  flexDirection: "row",
});

const PrimaryTextStyles = ({ variant, color }: ChipStylesProps): TextStyle => ({
  color: variant === ChipVariants.Filled ? "#FFFFFF" : color,
  fontWeight: "600",
});

const SecondaryTextStyles = ({
  variant,
  color,
}: ChipStylesProps): TextStyle => ({
  color: variant === ChipVariants.Filled ? "#FFFFFF" : color,
});

interface ChipProps extends ChipStylesProps {
  label: string;
  value: string;
}

export const Chip = ({ variant, color, label, value }: ChipProps) => {
  return (
    <View style={ChipStyles({ variant, color })}>
      <Text style={PrimaryTextStyles({ variant, color })}>{label} </Text>
      <Text style={SecondaryTextStyles({ variant, color })}>{value}</Text>
    </View>
  );
};
