import React from "react";
import { View, ViewStyle } from "react-native";
import { ColorsType } from "../styles/colors";
import { Chip, ChipVariants } from "./chip";

interface ChipsProps {
  PM10: number | string;
  PM25: number | string;
  Temperature: number | string;
  Humidity: number | string;
  variant: ColorsType;
}

export const Chips = ({
  variant,
  PM10,
  PM25,
  Temperature,
  Humidity,
}: ChipsProps) => {
  return (
    <View style={ChipContainerStyle}>
      <Chip
        label={`${Temperature}°C`}
        value={`${Humidity}%`}
        variant={ChipVariants.Outlined}
        color={variant}
      />
      <Chip
        label="PM10"
        value={`${PM10}`}
        variant={ChipVariants.Filled}
        color={variant}
      />
      <Chip
        label="PM2.5"
        value={`${PM25}`}
        variant={ChipVariants.Filled}
        color={variant}
      />
    </View>
  );
};

const ChipContainerStyle: ViewStyle = {
  display: "flex",
  flexDirection: "row",
  paddingTop: 10,
};
