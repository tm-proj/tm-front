import { Text } from "react-native";
import React from "react";
import { useFonts, Roboto_500Medium } from "@expo-google-fonts/roboto";
import { COLORS, ColorsType } from "./../styles/colors";

interface HeadingProps {
  children: JSX.Element | string;
  variant?: ColorsType;
}

export const Heading = ({ children, variant = COLORS.GOOD }: HeadingProps) => {
  const [fontLoaded] = useFonts({
    Roboto_500Medium,
  });

  if (!fontLoaded) return <Text>Loading...</Text>;

  return <Text style={headingStyle({ variant })}>{children}</Text>;
};

const headingStyle = ({ variant }: Pick<HeadingProps, "variant">) => ({
  fontSize: 24,
  fontFamily: "Roboto_500Medium",
  color: variant,
  marginBottom: 16,
});
