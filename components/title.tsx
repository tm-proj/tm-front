import { Text, TextStyle, View } from "react-native";
import React from "react";
import { useFonts, Roboto_500Medium } from "@expo-google-fonts/roboto";
import { COLORS, ColorsType } from "./../styles/colors";

interface TitleProps {
  children?: JSX.Element | JSX.Element[] | string | string[];
  right?: JSX.Element;
  variant?: ColorsType;
}

export const Title = ({
  children,
  right,
  variant = COLORS.GOOD,
}: TitleProps) => {
  const [fontLoaded] = useFonts({
    Roboto_500Medium,
  });

  if (!fontLoaded) return <Text>Loading...</Text>;

  return (
    <View style={{ minHeight: 90 }}>
      <View
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          marginTop: 16,
          marginBottom: 16,
        }}
      >
        <Text style={titleStyle({ variant })}>{children}</Text>
        {right}
      </View>
    </View>
  );
};

const titleStyle = ({ variant }: Pick<TitleProps, "variant">): TextStyle => ({
  fontSize: 36,
  fontFamily: "Roboto_500Medium",
  color: variant,
  flexWrap: "wrap",
  flex: 1,
});
