import { View, ViewStyle } from "react-native";
import { COLORS, ColorsType } from "../styles/colors";
import { transparentize } from "polished";
import React from "react";

interface PageProps {
  variant?: ColorsType;
  children:
    | JSX.Element
    | JSX.Element[]
    | null
    | boolean
    | (JSX.Element | boolean | null | undefined)[];
}

export const Page = ({ variant = COLORS.GOOD, children }: PageProps) => {
  const pageStyle = getStyles({ variant });

  return <View style={pageStyle}>{children}</View>;
};

type StylesProps = Required<Pick<PageProps, "variant">>;

const getStyles = ({ variant }: StylesProps): ViewStyle => ({
  backgroundColor: transparentize(0.75, variant),
  paddingTop: 36,
  paddingBottom: 18,
  paddingLeft: 18,
  paddingRight: 18,
  display: "flex",
  flexDirection: "column",
  flex: 1,
});
