import React, {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useState,
} from "react";
import { PermissionsAndroid } from "react-native";
import { BleError, BleManager, Device } from "react-native-ble-plx";

type ContextProps = {
  setManager: React.Dispatch<React.SetStateAction<BleManager | undefined>>;
  setConnectedDevice: React.Dispatch<React.SetStateAction<Device | undefined>>;
  manager?: BleManager;
  connectedDevice?: Device;
};

const BluetoothContext = createContext<ContextProps>({
  setManager: (_manager) => {
    /**
     * Placeholder Fn
     */
  },
  setConnectedDevice: (_device) => {
    /**
     * Placeholder Fn
     */
  },
});

type Props = {
  children: JSX.Element;
};

export const BluetoothProvider = ({ children }: Props) => {
  const [manager, setManager] = useState<BleManager>();
  const [connectedDevice, setConnectedDevice] = useState<Device>();

  return (
    <BluetoothContext.Provider
      value={{
        manager,
        setManager,
        connectedDevice,
        setConnectedDevice,
      }}
    >
      {children}
    </BluetoothContext.Provider>
  );
};

export const useBluetoothManager = () => {
  const { manager, setManager, connectedDevice, setConnectedDevice } =
    useContext(BluetoothContext);
  const [hasPermissions, setHasPermissions] = useState(false);
  const [lastError, setLastError] = useState<string>();

  const grantPermissions = useCallback(async () => {
    const status = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
    );
    setHasPermissions(status === PermissionsAndroid.RESULTS.GRANTED);
  }, []);

  const initialize = useCallback(() => {
    try {
      setManager((bt) => (!bt ? new BleManager() : bt));
    } catch (error) {
      setLastError(error);
    }
  }, []);

  const connect = useCallback(
    async (deviceId: string) => {
      if (manager) {
        try {
          const device = await (
            await manager.connectToDevice(deviceId)
          ).discoverAllServicesAndCharacteristics();
          setConnectedDevice(device);
        } catch (error) {
          setLastError(error);
        }
      }
    },
    [manager]
  );

  const disconnect = useCallback(async () => {
    if (manager && connectedDevice) {
      try {
        await manager.cancelDeviceConnection(connectedDevice.id);
      } catch (error) {
        setLastError(error);
      }
    }
  }, [manager, connectedDevice]);

  const startScan = useCallback(
    async (
      callback: (error: BleError | null, device: Device | null) => void
    ) => {
      if (manager) {
        manager.startDeviceScan(
          ["2aae8a3d-8c12-4a07-b05f-d101c8cb8a58"],
          null,
          callback
        );
      }
    },
    [manager]
  );

  const stopScan = useCallback(() => {
    if (manager) {
      manager.stopDeviceScan();
    }
  }, [manager]);

  return {
    manager,
    initialize,
    grantPermissions,
    startScan,
    stopScan,
    connect,
    disconnect,
    connectedDevice,
    hasPermissions,
    lastError,
    setLastError,
  };
};
