import { useToken } from "@hooks/useToken";
import axios from "axios";
import { useEffect, useMemo } from "react";
import useSWR from "swr";
import { useDevices } from "./useDevices";

const API_URL = "https://tmback.hopto.org:443";

type Measure = {
  utcUnixTimestamp: number;
  temperature: number;
  humidity: number;
  pm2_5: number;
  pm10: number;
};

type Device = {
  deviceId: number;
  name: string;
  measures: Measure[];
};

type DevicesDTO = {
  devices: Device[];
};

const detailsFetcher = async (token: string, deviceIds: number[]) =>
  await axios
    .post(
      `${API_URL}/mobile/get-measures`,
      {
        deviceIds,
        latestNum: 1,
      },
      {
        headers: {
          "x-access-token": token,
        },
      }
    )
    .then((res) => res.data);

export const useDevicesOverview = () => {
  const { token } = useToken();
  const { devices, error: devIdError } = useDevices();
  const devIds = useMemo(() => devices?.map((dev) => dev.deviceId), [devices]);
  const { data, error } = useSWR<DevicesDTO>([token, devIds], detailsFetcher);

  return { devices: data?.devices, error: error ?? devIdError };
};
