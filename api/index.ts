import axios from "axios";
import useSWR from "swr";

const API_URLS = {
  MAIN_DATA: "/data",
} as const;

const mainScreenResponse = [
  {
    name: "Test Dev",
    id: 1,
    PM10: 30,
    PM25: 22,
    temperature: 22,
    humidity: 45.633,
    timestamp: 1620954473,
  },
  {
    name: "Test Dev",
    id: 2,
    PM10: 70,
    PM25: 22,
    temperature: 22,
    humidity: 45.633,
    timestamp: 1620954473,
  },
  {
    name: "Test Dev",
    id: 3,
    PM10: 170,
    PM25: 22,
    temperature: 22,
    humidity: 45.633,
    timestamp: 1620954473,
  },
  {
    name: "Test Dev",
    id: 4,
    PM10: 270,
    PM25: 22,
    temperature: 22,
    humidity: 45.633,
    timestamp: 1620954473,
  },
  {
    name: "Test Dev",
    id: 5,
    PM10: 370,
    PM25: 22,
    temperature: 22,
    humidity: 45.633,
    timestamp: 1620954473,
  },
  {
    name: "Test Dev",
    id: 6,
    PM10: 450,
    PM25: 22,
    temperature: 22,
    humidity: 45.633,
    timestamp: 1620954473,
  },
];

export const fetcher = (url: string) => {
  switch (url) {
    case API_URLS.MAIN_DATA:
      return Promise.resolve(mainScreenResponse);
    default:
      return Promise.resolve(`example response for ${url}`);
  }
};

export const useMainData = () => useSWR(API_URLS.MAIN_DATA, fetcher);
