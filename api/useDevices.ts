import { useToken } from "@hooks/useToken";
import axios from "axios";
import useSWR from "swr";

type Device = {
  deviceId: number;
  name: string;
};

type DevicesDTO = {
  devices: Device[];
};

const API_URL = "https://tmback.hopto.org:443";

const devicesFetcher = async (token: string) =>
  await axios
    .get(`${API_URL}/mobile/get-devices`, {
      headers: {
        "x-access-token": token,
      },
    })
    .then((res) => res.data);

export const useDevices = () => {
  const { token } = useToken();
  const {
    data: devices,
    error,
    mutate,
  } = useSWR<DevicesDTO>(token, devicesFetcher);

  return { devices: devices?.devices, error, mutate };
};
