import { COLORS } from "@styles/colors";

const DISPLAY_NAME = {
  GOOD: "Good",
  MODERATE: "Moderate",
  LIGHT: "Light",
  UNHEALTHY: "Unhealthy",
  VERY_UNHEALTHY: "Very Unhealthy",
  HAZARDOUS: "Hazardous",
} as const;

interface MesaurementEntry {
  concentrationLow: number;
  concentrationHigh: number;
  AQILow: number;
  AQIHigh: number;
  displayName: string;
}

export const PM10Table = {
  [DISPLAY_NAME.GOOD]: {
    concentrationLow: 0,
    concentrationHigh: 54,
    AQILow: 0,
    AQIHigh: 50,
    displayName: DISPLAY_NAME.GOOD,
  } as MesaurementEntry,
  [DISPLAY_NAME.LIGHT]: {
    concentrationLow: 55,
    concentrationHigh: 154,
    AQILow: 51,
    AQIHigh: 100,
    displayName: DISPLAY_NAME.LIGHT,
  } as MesaurementEntry,
  [DISPLAY_NAME.MODERATE]: {
    concentrationLow: 155,
    concentrationHigh: 254,
    AQILow: 101,
    AQIHigh: 150,
    displayName: DISPLAY_NAME.MODERATE,
  } as MesaurementEntry,
  [DISPLAY_NAME.UNHEALTHY]: {
    concentrationLow: 255,
    concentrationHigh: 354,
    AQILow: 151,
    AQIHigh: 200,
    displayName: DISPLAY_NAME.UNHEALTHY,
  } as MesaurementEntry,
  [DISPLAY_NAME.VERY_UNHEALTHY]: {
    concentrationLow: 355,
    concentrationHigh: 424,
    AQILow: 201,
    AQIHigh: 300,
    displayName: DISPLAY_NAME.VERY_UNHEALTHY,
  } as MesaurementEntry,
  [DISPLAY_NAME.HAZARDOUS]: {
    concentrationLow: 425,
    concentrationHigh: 604,
    AQILow: 301,
    AQIHigh: 500,
    displayName: DISPLAY_NAME.HAZARDOUS,
  } as MesaurementEntry,
} as const;

type TableType = typeof PM10Table;

export const calculateAQI = (
  measurement: number,
  table: TableType = PM10Table
) => {
  const range = Object.values(table).find(
    (variant) =>
      measurement <= variant.concentrationHigh &&
      measurement >= variant.concentrationLow
  );

  if (range) {
    return Math.round(
      ((range.AQIHigh - range.AQILow) /
        (range.concentrationHigh - range.concentrationLow)) *
        (measurement - range.concentrationLow) +
        range.AQILow
    );
  }

  return 42;
};

export const getVariant = (
  concentration: number,
  table: TableType = PM10Table
) => {
  const range = Object.values(table).find(
    (variant) =>
      concentration <= variant.concentrationHigh &&
      concentration >= variant.concentrationLow
  );

  if (range) {
    switch (range.displayName) {
      case DISPLAY_NAME.GOOD:
        return COLORS.GOOD;
      case DISPLAY_NAME.HAZARDOUS:
        return COLORS.HAZARDOUS;
      case DISPLAY_NAME.MODERATE:
        return COLORS.MODERATE;
      case DISPLAY_NAME.LIGHT:
        return COLORS.LIGHT;
      case DISPLAY_NAME.LIGHT:
        return COLORS.UNHEALTHY;
      case DISPLAY_NAME.VERY_UNHEALTHY:
        return COLORS.VERY_UNHEALTHY;
    }
  }

  return COLORS.GOOD;
};

export const getSummaryText = (aqi: number, table: TableType = PM10Table) => {
  const range = Object.values(table).find(
    (variant) => aqi <= variant.AQIHigh && aqi >= variant.AQILow
  );

  if (range) {
    switch (range.displayName) {
      case DISPLAY_NAME.GOOD:
        return "Jakość powietrza jest dobra";
      case DISPLAY_NAME.HAZARDOUS:
        return "Powietrze jest bardzo szkodliwe";
      case DISPLAY_NAME.MODERATE:
        return "Jakość powietrza jest zła";
      case DISPLAY_NAME.UNHEALTHY:
        return "Jakość powietrza jest bardzo zła";
      case DISPLAY_NAME.LIGHT:
        return "Jakość powietrza nie jest zbyt dobra";
      case DISPLAY_NAME.VERY_UNHEALTHY:
        return "Jakość powietrza szkodliwa";
    }
  }

  return "Nieznana jakość powietrza";
};
