import { useBluetoothManager } from "@providers";

import { Device } from "react-native-ble-plx";

import { useCallback, useEffect, useState } from "react";

import { decode, encode } from "base-64";

/**
 * @returns
 */
export const useBluetooth = () => {
  const {
    manager,
    initialize,
    grantPermissions,
    startScan: _startScan,
    stopScan,
    connect,
    disconnect,
    connectedDevice,
    hasPermissions,
    lastError,
    setLastError,
  } = useBluetoothManager();

  const [devices, setDevices] = useState<Device[]>([]);

  useEffect(() => {
    if (!manager && hasPermissions) initialize();
  }, [manager, initialize, hasPermissions]);

  const writeConfig = useCallback(
    async (config) => {
      if (manager && connectedDevice) {
        try {
          const services = await manager.servicesForDevice(connectedDevice.id);
          const service = services.find(
            (service) => service.uuid === "2aae8a3d-8c12-4a07-b05f-d101c8cb8a58"
          );

          if (service) {
            const characteristics = await service.characteristics();
            const characteristic = characteristics.find(
              (c) => c.uuid === "38643c48-7ecc-422a-acf0-ec673bfa88a7"
            );

            await characteristic?.writeWithResponse(
              encode(JSON.stringify(config))
            );
          }
        } catch (error) {
          setLastError(error);
        }
      }
    },
    [manager, connectedDevice]
  );

  const notify = useCallback(
    async (callback: (value: string) => void) => {
      if (manager && connectedDevice) {
        try {
          const services = await manager.servicesForDevice(connectedDevice.id);
          const service = services.find(
            (service) => service.uuid === "2aae8a3d-8c12-4a07-b05f-d101c8cb8a58"
          );

          if (service) {
            const characteristics = await service.characteristics();
            const characteristic = characteristics.find(
              (c) => c.uuid === "b523613e-8963-4018-93ef-8ce799958cf4"
            );

            return characteristic?.monitor((error, characteristic) => {
              if (error) setLastError(error.message);
              if (characteristic?.value) {
                callback(decode(characteristic?.value));
              }
            });
          }
        } catch (error) {
          setLastError(error);
        }
      }
    },
    [manager, connectedDevice]
  );

  const startScan = useCallback(async () => {
    if (manager) {
      _startScan((error, device) => {
        if (error) setLastError(error.message);
        if (device)
          setDevices((devices) =>
            devices.find((x) => x.id === device.id)
              ? devices
              : [...devices, device]
          );
      });
    }
  }, [manager, _startScan]);

  return {
    lastError,
    stopScan,
    devices,
    connectedDevice,
    connect,
    disconnect,
    startScan,
    writeConfig,
    notify,
    hasPermissions,
    grantPermissions,
  };
};
