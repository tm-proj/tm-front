import { useCallback, useEffect, useReducer } from "react";
import { useStorage } from "@providers/StorageProvider";
import { useIsFocused } from "@react-navigation/native";

type State = { updateCount: number; deviceId: number };
type Action =
  | { type: "update" }
  | { type: "reset" }
  | { type: "set"; payload: number };

const initialState: State = {
  updateCount: 0,
  deviceId: -1,
};

const reducer = (state: State, action: Action): State => {
  switch (action.type) {
    case "update":
      return { ...state, updateCount: state.updateCount + 1 };
    case "reset":
      return initialState;
    case "set":
      return { ...state, deviceId: action.payload };
    default:
      throw new Error("Unknown action type");
  }
};

export const useFavouriteDevice = () => {
  const isFocused = useIsFocused();
  const [{ updateCount, deviceId }, dispatch] = useReducer(
    reducer,
    initialState
  );
  const { storage } = useStorage();

  useEffect(() => {
    if (isFocused) {
      getFavourite().then(({ deviceId }) => {
        console.log("Updated");
        dispatch({ type: "set", payload: deviceId });
      });
    }
  }, [updateCount, isFocused]);

  const setFavourite = useCallback(
    async (deviceId: number) => {
      await storage.save({
        key: "favDeviceId",
        data: deviceId,
        expires: null,
      });
      dispatch({ type: "update" });
    },
    [storage]
  );

  const getFavourite = useCallback(async () => {
    try {
      const deviceId = await storage.load<number>({
        key: "favDeviceId",
      });

      return { deviceId };
    } catch (error) {
      console.log(error.message);
      console.log(error.name);

      return { deviceId: -1, error };
    }
  }, [storage]);

  return { setFavourite, getFavourite, favourite: deviceId };
};
