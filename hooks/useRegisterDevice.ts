import axios from "axios";

import { useCallback } from "react";

import { useToken } from "./useToken";

const API_URL = "https://tmback.hopto.org:443";

export const useRegisterDevice = () => {
  const { token } = useToken();

  const register = useCallback(
    async (name: string) => {
      try {
        const { data } = await axios.post(
          `${API_URL}/mobile/preinit-device`,
          {
            name,
          },
          {
            headers: {
              "x-access-token": token,
            },
          }
        );
        return { deviceAccessToken: data.deviceAccessToken };
      } catch (error) {
        return { error: error.response };
      }
    },
    [token]
  );

  return { register };
};
