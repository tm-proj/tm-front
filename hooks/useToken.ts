import { useStorage } from "@providers/StorageProvider";

import axios, { AxiosError } from "axios";

import { useCallback, useEffect, useReducer } from "react";

type State = { updateCount: number; token: string; error?: AxiosError };
type Action =
  | { type: "update" }
  | { type: "reset" }
  | { type: "set"; payload: { error?: AxiosError; token: string } };

const initialState: State = {
  updateCount: 0,
  token: "",
};

const reducer = (state: State, action: Action): State => {
  switch (action.type) {
    case "update":
      return { ...state, updateCount: state.updateCount + 1 };
    case "reset":
      return initialState;
    case "set":
      return {
        ...state,
        token: action.payload.token,
        error: action.payload.error,
      };
    default:
      throw new Error("Unknown action type");
  }
};

const LOGIN_URL = "https://tmback.hopto.org:443/mobile/auth/signin";

type Token = {
  token: string;
};

export const useToken = () => {
  const [{ updateCount, token, error }, dispatch] = useReducer(
    reducer,
    initialState
  );
  const { storage } = useStorage();

  useEffect(() => {
    getToken().then(({ token, error }) => {
      dispatch({ type: "set", payload: { token, error } });
    });
  }, [updateCount]);

  const logout = useCallback(async () => {
    await storage.remove({ key: "token" });
  }, [storage]);

  const login = useCallback(async (login, password) => {
    try {
      const {
        data: { token },
      } = await axios.post<Token>(LOGIN_URL, {
        login,
        pass: password,
      });

      await storage.save({
        key: "token",
        data: token,
      });

      dispatch({ type: "update" });

      return true;
    } catch (error) {
      return false;
    }
  }, []);

  const getToken = useCallback(async () => {
    try {
      const token = await storage.load<string>({
        key: "token",
      });

      return { token };
    } catch (error) {
      console.log(error.message);
      console.log(error.name);

      return { token: "", error };
    }
  }, [storage]);

  return { token, error, login, logout };
};
