import axios from "axios";

import useSWR from "swr";

import { useToken } from "./useToken";

const API_URL = "https://tmback.hopto.org:443";

type Measure = {
  utcUnixTimestamp: number;
  temperature: number;
  humidity: number;
  pm2_5: number;
  pm10: number;
};

type Device = {
  deviceId: number;
  name: string;
  measures: Measure[];
};

type DevicesDTO = {
  devices: Device[];
};

const detailsFetcher = async (token: string, id: number) =>
  await axios
    .post(
      `${API_URL}/mobile/get-measures`,
      {
        deviceIds: [id],
        latestNum: 8,
      },
      {
        headers: {
          "x-access-token": token,
        },
      }
    )
    .then((res) => res.data);

export const useDeviceData = (id: number) => {
  const { token } = useToken();
  const { data, error } = useSWR<DevicesDTO>([token, id], detailsFetcher, {
    refreshInterval: 15000,
  });

  return { devices: data?.devices, error: error };
};
