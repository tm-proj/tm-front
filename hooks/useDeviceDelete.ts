import axios from "axios";

import { useCallback } from "react";

import { useToken } from "./useToken";

const API_URL = "https://tmback.hopto.org:443";

export const useDeviceDelete = () => {
  const { token } = useToken();
  const deleteDevice = useCallback(async (id: number) => {
    try {
      const { status } = await axios.delete(`${API_URL}/mobile/delete-device`, {
        data: {
          deviceId: id,
        },
        headers: {
          "x-access-token": token,
        },
      });

      return status === 200;
    } catch (error) {
      return false;
    }
  }, []);

  return { deleteDevice };
};
