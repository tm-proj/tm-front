export const COLORS = {
  GOOD: "#0091E2",
  LIGHT: "#FF9900",
  MODERATE: "#E25100",
  UNHEALTHY: "#E22900",
  VERY_UNHEALTHY: "#A80000",
  HAZARDOUS: "#540000",
} as const;

export type ColorsType = typeof COLORS[keyof typeof COLORS];
