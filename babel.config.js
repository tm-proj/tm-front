module.exports = function (api) {
  api.cache(true);
  return {
    presets: ["babel-preset-expo"],
    plugins: [
      ["module-resolver",
      {
        "alias": {
          "@api": "./api",
          "@components": "./components",
          "@views": "./views",
          "@icons": "./components/icons",
          "@styles": "./styles",
          "@utils": "./utils",
          "@hooks": "./hooks",
          "@providers": "./providers"
        },
      }],
    ],
  };
};
