import { Dimensions, View } from "react-native";
import { Page } from "@components/page";
import React, { useEffect, useMemo } from "react";
import { Title } from "@components/title";
import { useNavigation } from "@react-navigation/native";
import { Ionicons } from "@expo/vector-icons";
import { LineChart } from "react-native-chart-kit";
import { darken, lighten } from "polished";
import { useDeviceData } from "@hooks/useDeviceData";
import { ScrollView } from "react-native-gesture-handler";
import { DateTime } from "luxon";
import { Heading } from "@components/heading";
import { Card } from "@components/card";
import { getVariant } from "@utils/aqi.calculator";
import { COLORS } from "@styles/colors";
import { isEmpty } from "lodash";

type Props = {
  route: {
    params: { id: number };
  };
};

export const DeviceDetail = ({
  route: {
    params: { id },
  },
}: Props) => {
  const { canGoBack, goBack } = useNavigation();
  const { devices: _devices, error } = useDeviceData(id);

  const measures = useMemo(
    () =>
      _devices
        ? _devices[0].measures.sort((a, b) =>
            a.utcUnixTimestamp < b.utcUnixTimestamp ? -1 : 1
          )
        : [],
    [_devices]
  );

  const variant = useMemo(
    () =>
      !isEmpty(measures)
        ? getVariant(measures[measures.length - 1].pm10)
        : COLORS.GOOD,
    [measures]
  );

  const Time = useMemo(
    () =>
      !isEmpty(measures)
        ? measures.map((x) =>
            DateTime.fromSeconds(x.utcUnixTimestamp).toFormat("HH:mm")
          )
        : [],
    [measures]
  );
  const PM2 = useMemo(
    () => (!isEmpty(measures) ? measures.map((x) => x.pm2_5) : []),
    [measures]
  );
  const PM10 = useMemo(
    () => (!isEmpty(measures) ? measures.map((x) => x.pm10) : []),
    [measures]
  );
  const Temperature = useMemo(
    () => (!isEmpty(measures) ? measures.map((x) => x.temperature) : []),
    [measures]
  );
  const Humidity = useMemo(
    () => (!isEmpty(measures) ? measures.map((x) => x.humidity) : []),
    [measures]
  );

  return (
    <Page variant={variant}>
      <Title
        variant={variant}
        right={
          <Ionicons
            name="close-circle-outline"
            size={28}
            color={variant}
            onPress={() => canGoBack() && goBack()}
          />
        }
      >
        {(_devices && _devices[0].name) ?? "Ładowanie..."}
      </Title>
      {_devices && (
        <Card
          disabled
          name={_devices[0].name}
          PM25={measures[measures.length - 1].pm2_5}
          PM10={measures[measures.length - 1].pm10}
          Temperature={measures[measures.length - 1].temperature}
          Humidity={measures[measures.length - 1].humidity}
          timestamp={measures[measures.length - 1].utcUnixTimestamp}
          variant={variant}
        />
      )}

      <ScrollView>
        {!isEmpty(PM2) && (
          <>
            <View style={{ marginTop: 16 }}>
              <Heading variant={variant}>PM2.5</Heading>
            </View>
            <LineChart
              data={{
                labels: Time.map((x) => x.toString()),
                legend: ["µg/m3"],
                datasets: [
                  {
                    data: PM2,
                  },
                ],
              }}
              width={Dimensions.get("window").width - 36} // from react-native
              height={200}
              yAxisLabel=""
              yAxisSuffix=""
              yAxisInterval={1} // optional, defaults to 1
              chartConfig={{
                propsForHorizontalLabels: {
                  dx: -16,
                },
                backgroundColor: variant,
                backgroundGradientFrom: lighten(0.2, variant),
                backgroundGradientTo: darken(0.001, variant),
                decimalPlaces: 1,
                color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                style: {
                  borderRadius: 0,
                },
                propsForDots: {
                  r: "6",
                  strokeWidth: "1",
                  stroke: variant,
                },
              }}
              bezier
              style={{
                marginVertical: 8,
                borderRadius: 16,
              }}
            />
          </>
        )}
        {!isEmpty(PM10) && (
          <>
            <View style={{ marginTop: 16 }}>
              <Heading variant={variant}>PM10</Heading>
            </View>
            <LineChart
              data={{
                labels: Time.map((x) => x.toString()),
                legend: ["µg/m3"],
                datasets: [
                  {
                    data: PM10,
                  },
                ],
              }}
              width={Dimensions.get("window").width - 36} // from react-native
              height={200}
              yAxisLabel=""
              yAxisSuffix=""
              yAxisInterval={1} // optional, defaults to 1
              chartConfig={{
                backgroundColor: variant,
                backgroundGradientFrom: lighten(0.2, variant),
                backgroundGradientTo: darken(0.001, variant),
                decimalPlaces: 1, // optional, defaults to 2dp
                color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                style: {
                  borderRadius: 16,
                },
                propsForDots: {
                  r: "6",
                  strokeWidth: "2",
                  stroke: variant,
                },
              }}
              bezier
              style={{
                marginVertical: 8,
                borderRadius: 16,
              }}
            />
          </>
        )}
        {!isEmpty(Temperature) && (
          <>
            <View style={{ marginTop: 16 }}>
              <Heading variant={variant}>Temperatura</Heading>
            </View>
            <LineChart
              data={{
                labels: Time.map((x) => x.toString()),
                legend: ["°C"],
                datasets: [
                  {
                    data: Temperature,
                  },
                ],
              }}
              width={Dimensions.get("window").width - 36} // from react-native
              height={200}
              yAxisLabel=""
              yAxisSuffix=""
              yAxisInterval={1} // optional, defaults to 1
              chartConfig={{
                backgroundColor: variant,
                backgroundGradientFrom: lighten(0.2, variant),
                backgroundGradientTo: darken(0.001, variant),
                decimalPlaces: 2, // optional, defaults to 2dp
                color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                style: {
                  borderRadius: 16,
                },
                propsForDots: {
                  r: "6",
                  strokeWidth: "2",
                  stroke: variant,
                },
              }}
              bezier
              style={{
                marginVertical: 8,
                borderRadius: 16,
              }}
            />
          </>
        )}
        {!isEmpty(Humidity) && (
          <>
            <View style={{ marginTop: 16 }}>
              <Heading variant={variant}>Wilgotność</Heading>
            </View>
            <LineChart
              data={{
                labels: Time.map((x) => x.toString()),
                legend: ["%"],
                datasets: [
                  {
                    data: Humidity,
                  },
                ],
              }}
              width={Dimensions.get("window").width - 36} // from react-native
              height={200}
              yAxisLabel=""
              yAxisSuffix=""
              yAxisInterval={1} // optional, defaults to 1
              chartConfig={{
                backgroundColor: variant,
                backgroundGradientFrom: lighten(0.2, variant),
                backgroundGradientTo: darken(0.001, variant),
                decimalPlaces: 1, // optional, defaults to 2dp
                color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                style: {
                  borderRadius: 16,
                },
                propsForDots: {
                  r: "6",
                  strokeWidth: "2",
                  stroke: variant,
                },
              }}
              bezier
              style={{
                marginVertical: 8,
                borderRadius: 16,
              }}
            />
          </>
        )}
      </ScrollView>
    </Page>
  );
};
