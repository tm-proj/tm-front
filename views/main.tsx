import React from "react";
import { StatusBar } from "expo-status-bar";
import { ScrollView, Text } from "react-native";
import { Card } from "@components/card";
import { TopMenu } from "@components/top.menu";
import { COLORS } from "@styles/colors";
import { commonStyle } from "@styles/common.style";
import { Page } from "@components/page";
import { useNavigation } from "@react-navigation/core";
import { Navigation } from "./navigation";
import { getVariant } from "@utils/aqi.calculator";
import { useDevicesOverview } from "@api/useDevicesOverview";
import { useFavouriteDevice } from "@hooks/useFavouriteDevice";

export const Main = () => {
  const { navigate } = useNavigation();
  const { favourite } = useFavouriteDevice();
  const { devices, error } = useDevicesOverview();

  const favDevice = devices && devices.find((x) => x.deviceId === favourite);

  return (
    <Page>
      <ScrollView
        style={commonStyle.container}
        contentContainerStyle={commonStyle.wrapper}
      >
        <StatusBar style="auto" />
        <TopMenu variant={COLORS.GOOD} />
        {error && <Text>Wystąpił błąd</Text>}
        {favDevice && (
          <Card
            name={favDevice.name}
            PM10={favDevice.measures[0]?.pm10}
            PM25={favDevice.measures[0]?.pm2_5}
            Humidity={favDevice.measures[0]?.humidity}
            Temperature={favDevice.measures[0]?.temperature}
            timestamp={favDevice.measures[0]?.utcUnixTimestamp}
            variant={getVariant(favDevice.measures[0]?.pm10)}
            onPress={() =>
              navigate(Navigation.DeviceDetail, { id: favDevice.deviceId })
            }
          />
        )}
        {devices &&
          devices
            .filter((x) => x.deviceId !== favourite)
            .map((val) => (
              <Card
                key={val.deviceId}
                name={val.name}
                PM10={val.measures[val.measures.length - 1]?.pm10}
                PM25={val.measures[val.measures.length - 1]?.pm2_5}
                timestamp={
                  val.measures[val.measures.length - 1]?.utcUnixTimestamp
                }
                Temperature={val.measures[val.measures.length - 1]?.temperature}
                Humidity={val.measures[val.measures.length - 1]?.humidity}
                variant={getVariant(
                  val.measures[val.measures.length - 1]?.pm10
                )}
                onPress={() =>
                  navigate(Navigation.DeviceDetail, { id: val.deviceId })
                }
              />
            ))}
      </ScrollView>
    </Page>
  );
};
