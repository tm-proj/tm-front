import { useDevices } from "@api/useDevices";

import { Page } from "@components/page";
import { Title } from "@components/title";
import Whitebox from "@components/icons/whitebox";
import { Heading } from "@components/heading";
import { Button } from "@components/button";
import { Filler } from "@components/filler";

import { Ionicons } from "@expo/vector-icons";

import { useFavouriteDevice } from "@hooks/useFavouriteDevice";
import { useDeviceDelete } from "@hooks/useDeviceDelete";
import { useToken } from "@hooks/useToken";

import { useNavigation } from "@react-navigation/core";

import { COLORS } from "@styles/colors";

import { Pressable, Text, View } from "react-native";

import React, { useState } from "react";

import { Modal, Portal } from "react-native-paper";

import { Navigation } from "./navigation";

export const Settings = () => {
  const { canGoBack, goBack, navigate } = useNavigation();
  const [deleteId, setDeleteId] = useState<number>();
  const { setFavourite, favourite } = useFavouriteDevice();
  const { devices, mutate } = useDevices();
  const { deleteDevice } = useDeviceDelete();
  const { logout } = useToken();

  return (
    <Page>
      <Portal>
        <Modal
          visible={Boolean(deleteId)}
          onDismiss={() => {
            setDeleteId(undefined);
          }}
          contentContainerStyle={{
            backgroundColor: "#FFF",
            padding: 22,
            margin: 22,
            borderRadius: 9,
          }}
        >
          <Heading>Potwierdź usunięcie</Heading>
          <Text>Czy na pewno chcesz to zrobić?</Text>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "stretch",
              justifyContent: "space-between",
              marginTop: 16,
            }}
          >
            <Button
              styles={{ flex: 1, marginRight: 6 }}
              color="#A8201A"
              text="Nie"
              onPress={() => {
                setDeleteId(undefined);
              }}
            />
            <Button
              styles={{ flex: 1, marginLeft: 6 }}
              text="Tak"
              onPress={async () => {
                if (deleteId) {
                  await deleteDevice(deleteId);
                  mutate();
                }
                setDeleteId(undefined);
              }}
            />
          </View>
        </Modal>
      </Portal>
      <Title
        right={
          <Ionicons
            name="close-circle-outline"
            size={28}
            color={COLORS.GOOD}
            onPress={() => canGoBack() && goBack()}
          />
        }
      >
        Ustawienia
      </Title>
      <Heading>Twoje urządzenia</Heading>
      <Whitebox>
        <View style={{ padding: 16 }}>
          {devices &&
            devices.map((device, index, arr) => (
              <View
                key={index}
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                  paddingTop: index === 0 ? 0 : 10,
                  borderBottomWidth: index === arr.length - 1 ? 0 : 1,
                  paddingBottom: index === arr.length - 1 ? 0 : 10,
                  borderBottomColor: COLORS.GOOD,
                }}
              >
                <Text style={{ fontFamily: "Roboto_400Regular", fontSize: 18 }}>
                  {device.name}
                </Text>
                <Pressable
                  style={{ display: "flex", flexDirection: "row" }}
                  onPress={async () => {
                    await setFavourite(device.deviceId);
                  }}
                >
                  {device.deviceId === favourite ? (
                    <Ionicons name="star" size={24} color="#FFB931" />
                  ) : (
                    <Ionicons name="star-outline" size={24} color="#FFB931" />
                  )}
                  <Ionicons
                    style={{ marginLeft: 10 }}
                    name="trash-outline"
                    size={24}
                    color={COLORS.GOOD}
                    onPress={() => setDeleteId(device.deviceId)}
                  />
                </Pressable>
              </View>
            ))}
        </View>
      </Whitebox>
      <Filler />
      <Button
        text="Wyloguj"
        onPress={async () => {
          await logout();
          navigate(Navigation.Login);
        }}
      />
    </Page>
  );
};
