import Whitebox from "@components/icons/whitebox";
import { Page } from "@components/page";
import { Title } from "@components/title";
import { Filler } from "@components/filler";
import { Button } from "@components/button";
import { Heading } from "@components/heading";
import { TextInput } from "@components/form/text.input";

import {
  useFonts,
  Roboto_500Medium,
  Roboto_400Regular,
} from "@expo-google-fonts/roboto";

import { Ionicons } from "@expo/vector-icons";

import { useBluetooth } from "@hooks/useBluetooth";
import { useRegisterDevice } from "@hooks/useRegisterDevice";

import { useNavigation } from "@react-navigation/native";

import { COLORS } from "@styles/colors";

import React, { useEffect } from "react";

import { View } from "react-native";

import { Formik } from "formik";

import { ScrollView } from "react-native-gesture-handler";

export const PairDeviceSettings = () => {
  const { canGoBack, goBack, addListener, navigate } = useNavigation();
  const { disconnect, connectedDevice, writeConfig, notify } = useBluetooth();
  const { register } = useRegisterDevice();

  useFonts({
    Roboto_500Medium,
    Roboto_400Regular,
  });

  useEffect(() => {
    const onBlur = addListener("blur", () => {
      console.log("onblur", connectedDevice);
      if (connectedDevice) disconnect();
    });

    return onBlur;
  }, [addListener, connectedDevice, disconnect]);

  return (
    <Page>
      <Formik
        initialValues={{ ssid: "", password: "", name: "" }}
        onSubmit={async (values) => {
          console.log({ values });

          const { error, deviceAccessToken } = await register(values.name);

          if (error) {
            console.log(error);
            return;
          }

          if (deviceAccessToken) {
            await writeConfig({
              token: deviceAccessToken,
              ssid: values.ssid,
              pwd: values.password,
            });

            await notify((value) => value === "200" && navigate("Main"));
          }
        }}
      >
        {({ submitForm }) => (
          <>
            <ScrollView>
              <Title
                right={
                  <Ionicons
                    name="close-circle-outline"
                    size={28}
                    color={COLORS.GOOD}
                    onPress={() => canGoBack() && goBack()}
                  />
                }
              >
                Skonfiguruj {"\n"}wybrane urządzenie
              </Title>
              <Heading>Dane urządzenia</Heading>
              <Whitebox>
                <View style={{ marginHorizontal: 8, marginTop: 6 }}>
                  <TextInput label="Nazwa urządzenia" name="name" />
                  <TextInput label="Nazwa sieci Wi-Fi" name="ssid" />
                  <TextInput label="Hasło Wi-Fi" name="password" />
                </View>
              </Whitebox>
            </ScrollView>
            <Filler />
            <Button
              styles={{ marginTop: 8 }}
              disabled={false}
              onPress={submitForm}
              text="Zapisz ustawienia"
            />
          </>
        )}
      </Formik>
    </Page>
  );
};
