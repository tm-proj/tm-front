import { Formik } from "formik";
import { Text, View } from "react-native";
import React from "react";
import { object, string } from "yup";
import {
  useFonts,
  Roboto_500Medium,
  Roboto_400Regular,
} from "@expo-google-fonts/roboto";

import { Page } from "@components/page";
import Whitebox, { Element } from "@components/icons/whitebox";
import { Title } from "@components/title";
import { Button } from "@components/button";
import { Filler } from "@components/filler";
import { TextInput } from "@components/form/text.input";
import { COLORS } from "@styles/colors";
import { Heading } from "@components/heading";
import { useNavigation } from "@react-navigation/core";
import { Navigation } from "./navigation";
import axios from "axios";

export const Register = () => {
  const navigation = useNavigation();
  const [fontLoaded] = useFonts({
    Roboto_500Medium,
    Roboto_400Regular,
  });

  if (!fontLoaded) return null;

  return (
    <Page>
      <Formik
        initialValues={{ login: "", password: "" }}
        validationSchema={object({
          login: string().required("Uzupełnij to pole"),
          password: string()
            .min(4, ({ min }) => `Wypełnij minimum ${min} znaki`)
            .max(32, ({ max }) => `Hasło może mieć maksymalnie ${max} znaki`)
            .required("Hasło jest wymagane"),
        })}
        onSubmit={async (values) => {
          try {
            await axios.post(
              `https://tmback.hopto.org:443/mobile/auth/signup`,
              { login: values.login, pass: values.password }
            );
          } catch (error) {
            console.log(error);
          }
          navigation.navigate(Navigation.Login);
        }}
      >
        {({ handleSubmit, isValid }) => (
          <>
            <Title>Rejestracja</Title>
            <Heading>Dane logowania</Heading>
            <Whitebox>
              <Element>
                <TextInput label="Login" name="login" />
              </Element>
              <View style={{ paddingHorizontal: 15, paddingBottom: 15 }}>
                <TextInput label="Hasło" name="password" secureTextEntry />
              </View>
            </Whitebox>
            <Filler />
            <View
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "center",
              }}
            >
              <Text
                onPress={() => navigation.goBack()}
                style={{
                  color: COLORS.GOOD,
                  fontFamily: "Roboto_500Medium",
                  fontSize: 16,
                  marginBottom: 12,
                  textAlign: "center",
                }}
              >
                {" "}
                Wróć do logowania
              </Text>
            </View>
            <Button
              disabled={!isValid}
              onPress={() => handleSubmit()}
              text="Rejestracja"
            />
          </>
        )}
      </Formik>
    </Page>
  );
};
