import { Formik, FormikHelpers } from "formik";
import { Text, View } from "react-native";
import React, { useCallback, useEffect } from "react";
import { object, string } from "yup";
import { Page } from "@components/page";
import Whitebox, { Element } from "@components/icons/whitebox";
import { Title } from "@components/title";
import { Button } from "@components/button";
import { Filler } from "@components/filler";
import { TextInput } from "@components/form/text.input";
import { COLORS } from "@styles/colors";
import { Heading } from "@components/heading";
import { useNavigation } from "@react-navigation/core";
import { Navigation } from "./navigation";
import { useToken } from "@hooks/useToken";
import { isEmpty } from "lodash";

const initialValues = { login: "", password: "" };
type Values = typeof initialValues;
const validationSchema = object({
  login: string().required("Uzupełnij to pole"),
  password: string()
    .min(4, ({ min }) => `Wypełnij minimum ${min} znaki`)
    .max(32, ({ max }) => `Hasło może mieć maksymalnie ${max} znaki`)
    .required("Hasło jest wymagane"),
});

export const Login = () => {
  const { navigate } = useNavigation();
  const { login, error, token } = useToken();

  useEffect(() => {
    if (!isEmpty(token) && !error) {
      console.log(token);
      navigate(Navigation.Main);
    }
  }, [error, token]);

  const onSubmit = useCallback(
    async (values: Values, formik: FormikHelpers<Values>) => {
      formik.setSubmitting(true);
      if (await login(values.login, values.password)) {
        navigate(Navigation.Main);
      } else {
        formik.setFieldError("login", "Błędne dane logowania");
      }

      formik.setSubmitting(false);
    },
    [navigate]
  );

  return (
    <Page>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
      >
        {({ handleSubmit, isValid, isSubmitting }) => (
          <>
            <Title>Logowanie</Title>
            <Heading>Dane logowania</Heading>
            <Whitebox>
              <Element>
                <TextInput label="Login" name="login" />
              </Element>
              <View style={{ paddingHorizontal: 15, paddingBottom: 15 }}>
                <TextInput label="Hasło" name="password" secureTextEntry />
              </View>
            </Whitebox>
            <Filler />
            <View
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "center",
              }}
            >
              <Text
                style={{
                  color: COLORS.GOOD,
                  fontFamily: "Roboto_400Regular",
                  fontSize: 16,
                  marginBottom: 12,
                  textAlign: "center",
                }}
              >
                Nie masz konta?
              </Text>
              <Text
                onPress={() => navigate("Register")}
                style={{
                  color: COLORS.GOOD,
                  fontFamily: "Roboto_500Medium",
                  fontSize: 16,
                  marginBottom: 12,
                  textAlign: "center",
                }}
              >
                {" "}
                Zarejestruj się!
              </Text>
            </View>
            <Button
              disabled={!isValid || isSubmitting}
              onPress={() => handleSubmit()}
              text="Logowanie"
            />
          </>
        )}
      </Formik>
    </Page>
  );
};
