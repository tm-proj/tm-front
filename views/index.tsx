import { DeviceDetail } from "./device.detail";
import { Login } from "./login";
import { Main } from "./main";
import { Settings } from "./settings";
import { Register } from "./register";
import { PairDeviceList } from "./pair.device.list";
import { PairDeviceSettings } from "./pair.device.settings";

interface View {
  toString: () => string;
  Component: () => JSX.Element;
  options?: object;
}

export const Views = {
  Main: {
    Component: Main,
    toString: () => "Main",
    options: {
      headerShown: false,
    },
  } as View,
  PairDeviceList: {
    Component: PairDeviceList,
    toString: () => "PairDeviceList",
  } as View,
  PairDeviceSettings: {
    Component: PairDeviceSettings,
    toString: () => "PairDeviceSettings",
  } as View,
  Login: {
    Component: Login,
    toString: () => "Login",
    options: {
      headerShown: false,
    },
  } as View,
  Register: {
    Component: Register,
    toString: () => "Register",
  },
  Settings: {
    Component: Settings,
    toString: () => "Settings",
  } as View,
  DeviceDetail: {
    Component: DeviceDetail,
    toString: () => "DeviceDetail",
  } as View,
};
