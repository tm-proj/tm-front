export const Paths = {
  Main: "Main",
  Login: "Login",
  Settings: "Settings",
  DeviceDetail: "DeviceDetail",
  PairDeviceList: "PairDeviceList",
  PairDeviceSettings: "PairDeviceSettings",
} as const;
