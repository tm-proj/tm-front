import Whitebox from "@components/icons/whitebox";
import { Page } from "@components/page";
import { Title } from "@components/title";
import { Filler } from "@components/filler";
import { Button } from "@components/button";
import { Heading } from "@components/heading";
import {
  useFonts,
  Roboto_500Medium,
  Roboto_400Regular,
} from "@expo-google-fonts/roboto";
import { Ionicons } from "@expo/vector-icons";
import { useBluetooth } from "@hooks/useBluetooth";
import { useIsFocused, useNavigation } from "@react-navigation/native";
import { COLORS } from "@styles/colors";
import React, { useEffect, useState } from "react";
import { Pressable, Text, View } from "react-native";
import { Device } from "react-native-ble-plx";
import { lighten } from "polished";
import { Paths } from "./paths";
import { ActivityIndicator } from "react-native-paper";

export const PairDeviceList = () => {
  const { canGoBack, goBack, navigate, addListener } = useNavigation();
  const {
    connect,
    devices,
    startScan,
    stopScan,
    hasPermissions,
    grantPermissions,
  } = useBluetooth();
  const [device, setDevice] = useState<Device>();
  const isFocused = useIsFocused();
  const [scanning, setScanning] = useState(true);

  useEffect(() => {
    if (isFocused) {
      startScan();
      setScanning(true);
    }
  }, [startScan, isFocused]);

  useEffect(() => {
    const onBlur = addListener("blur", () => {
      setDevice(undefined);
      stopScan();
    });

    return onBlur;
  }, [addListener]);

  useEffect(() => {
    if (!hasPermissions) grantPermissions();
  }, [grantPermissions, hasPermissions]);

  useFonts({
    Roboto_500Medium,
    Roboto_400Regular,
  });

  return (
    <Page>
      <Title
        right={
          <Ionicons
            name="close-circle-outline"
            size={28}
            color={COLORS.GOOD}
            onPress={() => canGoBack() && goBack()}
          />
        }
      >
        Urządzenia {"\n"}w pobliżu
      </Title>
      <Heading>Wybierz jedno z listy</Heading>
      {scanning && <ActivityIndicator animating style={{ marginBottom: 16 }} />}
      <Whitebox>
        <View>
          {devices.length === 0 && (
            <Text
              style={{
                fontFamily: "Roboto_400Regular",
                fontSize: 18,
                padding: 15,
              }}
            >
              Szukanie urządzeń
            </Text>
          )}
          {devices.map((_device, index, arr) => (
            <Pressable
              android_ripple={{ color: lighten(0.1, COLORS.GOOD) }}
              key={_device.id}
              onPress={() => {
                if (device?.id === _device.id) {
                  setDevice(undefined);
                } else {
                  setDevice(_device);
                }
              }}
            >
              <View
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                  borderBottomWidth: index === arr.length - 1 ? 0 : 1,
                  borderBottomColor: COLORS.GOOD,
                  height: 52,
                  padding: 15,
                }}
              >
                <Text style={{ fontFamily: "Roboto_400Regular", fontSize: 18 }}>
                  {_device.name}
                </Text>
                {_device.id === device?.id && (
                  <Ionicons
                    name="checkmark-outline"
                    size={28}
                    color={COLORS.GOOD}
                    onPress={() => canGoBack() && goBack()}
                  />
                )}
              </View>
            </Pressable>
          ))}
        </View>
      </Whitebox>
      <Filler />
      <Button
        disabled={!device}
        onPress={async () => {
          if (device) {
            try {
              await connect(device.id);
              navigate(Paths.PairDeviceSettings);
            } catch (e) {
              console.warn(e);
            }
          }
        }}
        text="Połącz"
      />
    </Page>
  );
};
